**Поднять проект**

В корне проекта запустить

`make dev-up`

**Запуск тестов**

`composer run test`

или, если нужна только одна группа

`composer run test -- --group=initialunittest`

Запускаются через композер, прочитать про запуск через композер можно здесь - https://getcomposer.org/doc/articles/scripts.md

Настройки пхпюнита лежат в phpunit.xml, почитать про них можно здесь - https://phpunit.de/manual/6.5/en/appendixes.configuration.html

