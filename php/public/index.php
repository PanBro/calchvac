<?php declare(strict_types=1);

use \App\Base\System;

require_once 'vendor/autoload.php';
define('ROOT', dirname(__FILE__));

$system = new System();
$system->run();

