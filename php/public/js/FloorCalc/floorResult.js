$("#formFloorCalc").submit(function(e) {
    e.preventDefault();
    let formFloorCalc = $("#formFloorCalc").serialize();
    $.ajax({
        url: 'floorcalcresult',
        method: 'POST',
        dataType: 'text',
        data: formFloorCalc,
        success: function(responseData){
            $("#blockajax").html(responseData);
        },
        error: function(responseData) {
            console.log(responseData);
            $("#blockajax").html('произошла ошибка!!!');
        }
    });
});


var input = document.getElementById('materialSelects');

input.oninput = function () {
    let material = input.value;
    let arrWithIDs = Array.from(document.getElementById('pipeSelects').querySelectorAll('select'));

    function som() {
        arrWithIDs.forEach(elem => {
            elem.style.display = 'none';
            console.log(elem);
        });
    }
    som(arrWithIDs);

    document.getElementById(material).style.display = '';
};