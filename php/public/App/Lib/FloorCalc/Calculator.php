<?php


namespace App\Lib\FloorCalc;

use App\Lib\FloorCalc\Calculators\CalcHeatingFlow;
use App\Lib\FloorCalc\Calculators\CalcTemperatures;
use App\Lib\FloorCalc\Calculators\ExtraCalculator;
use App\Lib\FloorCalc\Calculators\Utils\MathUtils;
use App\Lib\FloorCalc\Exceptions\WrongDataException;


/**
 * производит сами вычисления
 * @package App\Lib\Calc\FloorCalc
 */
class Calculator
{
    /** @var DataContainer */
    private $data;

    /** @var CalcHeatingFlow  */
    private $heatingFlow;

    /** @var MathUtils  */
    private $mathUtils;

    /** @var CalcTemperatures  */
    private $temperatureCalculations;

    /**
     * Calculators constructor.
     * @param DataContainer $data
     */
    public function __construct(DataContainer $data)
    {
        $this->data = $data;

        $this->heatingFlow = new CalcHeatingFlow($this->data);

        $this->mathUtils = new MathUtils();

        $this->temperatureCalculations = new CalcTemperatures($this->heatingFlow);

        $this->extraCalculator = new ExtraCalculator($this->data);
    }

    /**
     * формирует массив значений для вывода основных результатов калькулятора
     * @return array
     * @throws WrongDataException
     */
    public function calculate(): array
    {
        $quotientHeatFlux = $this->heatingFlow->calcQuotientHeatFlux();
        $heatFlowUpwardly = $this->heatingFlow->getHeatFlowUpwardly($quotientHeatFlux);
        $calcMaxFloorTemperature = $this->temperatureCalculations->calcMaxFloorTemperature(
            $quotientHeatFlux,
            $heatFlowUpwardly
        );
        $minFloorTemperature = $this->temperatureCalculations->calcMinFloorTemperature(
            $calcMaxFloorTemperature,
            $this->heatingFlow->calcAngle()
        );
        $downwardHeatFlow = $this->temperatureCalculations->calcDownwardHeatFlow(
            $heatFlowUpwardly,
            $quotientHeatFlux
        );
        $totalSpecificHeatFlux = $this->temperatureCalculations->calcTotalSpecificHeatFlux(
            $heatFlowUpwardly,
            $downwardHeatFlow
        );
        return [

            //ОСНОВНЫЕ РАСЧЕТЫ
            'heatFlowUpwardly' => $heatFlowUpwardly,
            'maxFloorTemperature' => $calcMaxFloorTemperature,
            'minFloorTemperature' => $minFloorTemperature,
            'averageFloorTemperature' => $this->temperatureCalculations->calcAverageFloorTemperature(
                $calcMaxFloorTemperature,
                $minFloorTemperature
            ),

//            'debug' => var_export(debug_backtrace(), true),

            //ПРОМЕЖУТОЧНЫЕ РАСЧЕТЫ
            'intermediateCalculationsChecked' => $this->data->getIntermediateCalculations(),


            'thermalResistUpperLayer' => $this->heatingFlow->calcSumThermalResist($this->data->getUpperLayer()),
            'thermalResistLowerLayer' => $this->heatingFlow->calcSumThermalResist($this->data->getLowerLayer()),
            'leadThermalResistUpperLayer' => $this->heatingFlow->calcLeadThermalResistUpperLayer(),
            'leadThermalResistLowerLayer' => $this->heatingFlow->calcLeadThermalResistLowerLayer(),
            'angleBetweenFloorAndMaxThermalResistance' => $this->heatingFlow->calcAngle(),
            'maxThermalResistanceOfLayersAbovePipes' => $this->heatingFlow->calcMaxThermalResistanceOfAboveLayers($this->heatingFlow->calcAngle()),
            'quotientHeatFlux' => $quotientHeatFlux,
            'leadThermResistanceOfPipes' => $this->heatingFlow->getLeadThermResistanceOfPipes(),

            'downwardHeatFlow' => $downwardHeatFlow,
            'totalSpecificHeatFlux' => $totalSpecificHeatFlux,
            'totalLinearHeatFlow' => $this->temperatureCalculations->calcTotalLinearHeatFlow(
                $totalSpecificHeatFlux
            ),

            //ДОПОЛНИТЕЛЬНЫЕ РАСЧЕТЫ
            'extraCalculationChecked' => $this->data->getExtraCalculations(),

//            'squareInSnakes' => $this->pipesMaterialAndSizes->getExternalDiameterOfPipes($this->data->getPipesMaterial(), $this->data->getPipesSize())['thickness'],
            'squareInSnakes' => $this->extraCalculator->calcSquareSnake(),

        ];
    }
}




























