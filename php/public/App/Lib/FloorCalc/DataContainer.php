<?php


namespace App\Lib\FloorCalc;

use phpDocumentor\Reflection\Types\Boolean;

/**
 * для хранения valueObject
 * @package App\Lib\Calc\FloorCalc
 */
class DataContainer
{
    private $post;

    public function __construct(array $post)
    {
        $this->post = $post;
    }

    //Общие данные

    /** Принимаемая температура теплоносителя @return int */
    public function getTempHeatCarrier(): ?int
    {
        return (int)($this->post['tempHeatCarrier'] ?? null);
    }

    /** Площадь контура теплого пола @return float */
    public function getSizeOfFloor(): ?float
    {
        return (float)($this->post['sizeOfFloor'] ?? null);
    }

    /** Температура воздуха в рассчитываемом помещении @return int */
    public function getTempInnerRoom(): ?int
    {
        return (int)($this->post['tempInnerRoom'] ?? null);
    }

    /** Температура в нижележащем помещении @return int */
    public function getTempLowerRoom(): ?int
    {
        return (int)($this->post['tempLowerRoom'] ?? null);
    }

    /** Инфомация о трубах: материал @return string */
    public function getPipesMaterial(): ?string
    {
        return (string)($this->post['materialSelects'] ?? null);
    }

    /** Инфомация о трубах: размеры @return array */
    public function getPipesSize(): ?array
    {
        return ($this->post['pipesSize'] ?? null);
    }

    /** Коэффициент теплоотдачи нижележащей горизонтальной поверхности @return float */
    public function getHeatRatioLowerSurface(): ?float
    {
        return (float)($this->post['heatRatioLowerSurface'] ?? null);
    }


    //Сведения о конструкции пола

    /** Принятый шаг труб (в сантиметрах) @return float */
    public function getPipeStep(): ?float
    {
        return (float)($this->post['pipeStep'] ?? null);
    }

    /** Коэффициент теплоотдачи пола @return float */
    public function getFloorHeatTransferRatio(): ?float
    {
        return (float)($this->post['floorHeatTransferRatio'] ?? null);
    }

    //Слои НАД трубами

    /** слои НАД трубами @return array */
    public function getUpperLayer(): ?array
    {
        return ($this->post['upperLayer'] ?? null);
    }

    //Слои ПОД трубами

    /** слой ПОД трубами @return array */
    public function getLowerLayer(): ?array
    {
        return ($this->post['lowerLayer'] ?? null);
    }

    //чекбоксы

    /** Отобразить промежуточные расчеты @return bool  */
    public function getIntermediateCalculations(): bool
    {
        return !empty($this->post['intermediateCalculations']);
    }

    /** Отобразить дополнительные расчеты @return bool */
    public function getExtraCalculations(): bool
    {
        return !empty($this->post['extraCalculation']);
    }


}

