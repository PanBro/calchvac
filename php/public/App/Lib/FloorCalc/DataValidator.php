<?php


namespace App\Lib\FloorCalc;

use App\Lib\Dictionaries\MaterialAndSizesOfPipes;

/**
 * Валидирует вводные данные
 * @package App\Lib\FloorCalc
 */
class DataValidator
{
    private $data;
    private $errors = [];

    /**
     * @param DataContainer $data
     */
    public function __construct(DataContainer $data)
    {
        $this->data = $data;
    }

    /**
     * Валидирует вводные данные
     * @return array|null
     */
    public function validate(): ?array
    {
        if ($this->data->getTempHeatCarrier() < 30) {
            $this->addError(
                'tempHeatCarrier',
                'Принимаемая температура теплоносителя должна быть не меньше чем 30 градусов (экономически не целесообразно).'
            );
        }

        if ($this->data->getSizeOfFloor() < 1) {
            $this->addError(
                'minSizeOfFloor',
                'Площадь контура тёплого пола не может быть меньше 1 метра квадратного.'
            );
        }

        $correctTempInnerRoom = (
            $this->data->getTempInnerRoom() > 18
            && $this->data->getTempInnerRoom() < 25
        );

        if (!$correctTempInnerRoom) {
            $this->addError(
                'tempInnerRoom',
                'Температура воздуха в рассчитываемом помещении должна быть в диапазоне от 18 градусов до 25 градусов.'
            );
        }

        if ($this->data->getTempHeatCarrier() <= $this->data->getTempInnerRoom()) {
            $this->addError(
                'tempHeatCarrier',
                'Принимаемая температура теплоносителя не может быть меньше температуры воздуха в рассчитываемом помещении.'
            );
        }

        $pipesDictionary = new MaterialAndSizesOfPipes();
        $upperLayers = $this->data->getUpperLayer();
        foreach ($upperLayers as $upperLayer) {
            if ((int)$upperLayer['thickness'] < 0) {
                $this->addError(
                    'negativeThicknessOfScreed',
                    'Толщина стяжки слоя пола НАД трубами не может быть отрицательной.'
                );
                break;
            }
        }

        $externalDiameter = $pipesDictionary->getExternalDiameterOfPipes($this->data->getPipesMaterial(), $this->data->getPipesSize());
        if (((int)$upperLayers[2]['thickness'] + ((int)$upperLayers[1]['thickness']) < (($externalDiameter + 45)) * 0.98)) {
        $errorText = <<<HTML
Толщина стяжки для укрытия трубопроводов (толщина слоя НАД трубами) должна быть не менее чем на 45 мм больше диаметра трубопроводов 
<a href="http://docs.cntd.ru/document/1200084091#P00CE" target="_blank">(СП 29.13330.2011).</a>
HTML;

            $this->addError(
                'thicknessOfScreed',
                $errorText
            );
        }

        $lowerLayers = $this->data->getLowerLayer();
        foreach ($lowerLayers as $lowerLayer) {
            if ((int)$lowerLayer['thickness'] < 0) {
                $this->addError(
                    'negativeThicknessOfScreed',
                    'Толщина стяжки слоя пола ПОД трубами не может быть отрицательной.'
                );
                break;
            }
        }

        if ($this->data->getPipeStep() < 8) {
            $this->addError(
                'pipeStep',
                'Принятый шаг труб не может быть принят менее 8 сантиметров.'
            );
        }

        return $this->errors ?: null;
    }

    /**
     * добавляет текст ошибки
     * @param $error
     */
    private function addError($error, $errorText): void
    {
        if (!isset($this->errors[$error])) {
            $this->errors[$error] = [];
        }
        $this->errors[$error][] = $errorText;

    }
}



