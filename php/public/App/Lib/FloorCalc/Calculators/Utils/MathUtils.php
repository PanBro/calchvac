<?php


namespace App\Lib\FloorCalc\Calculators\Utils;


use App\Lib\Dictionaries\DictionaryManager;
use App\Lib\FloorCalc\Exceptions\WrongDataException;

class MathUtils
{
    /** @var int перевод миллиметров в метры  */
    public const MM2M  = 1000;

    /** @var DictionaryManager */
    private $dictionaryManager;

    public function __construct()
    {
        $this->dictionaryManager = DictionaryManager::getInstance();
    }

    /**
     * считает термическое сопротивления заданного слоя
     * @param int $thickness - толщина слоя пола
     * @param float|null $ratio - коэффициент термического сопротивления
     * @return float
     * @throws WrongDataException
     */
    public function calcThermalResist(int $thickness, ?float $ratio): float
    {
        if (empty($ratio)) {
            throw new WrongDataException('не указан коэффициент термического сопротивления');
        }
        return $thickness / self::MM2M / $ratio;
    }

    /**
     * считает суммарную толщину всех слоёв пола НАД трубами
     * @param array $layers - массив значений слоя над трубами
     * @return int
     */
    public function calcSumThicknessOfUpperLayer(array $layers): int
    {
        $sumThickness = 0;

        foreach ($layers as $layer) {
            $sumThickness += (int)$layer['thickness'];
        }

        return $sumThickness;
    }

    /**
     * выбирает значение коэффициента термического сопротивления
     * @param float|null $byUser - значение коэффициента, введённое пользователем (если есть)
     * @param string $fromDictionary - массив справочных значений для выбора коффициента
     * @return float|null
     */
    public function chooseThermalConductivityRatio($byUser, string $fromDictionary): ?float
    {
        if ($byUser != 0) {
            return (float)$byUser;
        }
        $thermalConductivityRatioOfLayers = $this->dictionaryManager->getThermalConductivityRatioOfLayers();
        return (float)($thermalConductivityRatioOfLayers->getRatioByMaterial($fromDictionary));
    }

    /**
     * Приведённое термическое сопротивление слоёв пола (НАД и ПОД трубами)
     * @param float $thermalResist
     * @param float $floorHeatTransferRatio
     * @return float|int
     * @throws WrongDataException

     */
    public function calcLeadThermalResistOfLayer(float $thermalResist, float $floorHeatTransferRatio): ?float
    {
        if (empty($floorHeatTransferRatio)) {
            throw new WrongDataException('не указан коэффициент теплоотдачи пола $floorHeatTransferRatio');
        }
        return $thermalResist + (1 / $floorHeatTransferRatio);
    }

}