<?php


namespace App\Lib\FloorCalc\Calculators;


use App\Lib\FloorCalc\DataContainer;

class ExtraCalculator
{

    private $data;

    /**
     * ExtraCalculator constructor.
     * @param DataContainer $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Площадь контура теплого пола, переведённая из метров квадратных в удавов квадратных
     * @return float
     */
    public function calcSquareSnake()
    {
        $squareInMeters = $this->data->getSizeOfFloor();
        $squareInSnake = $squareInMeters / 4.6;

        return round($squareInSnake, 2);
    }
}