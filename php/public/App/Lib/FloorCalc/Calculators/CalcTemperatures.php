<?php


namespace App\Lib\FloorCalc\Calculators;

use App\Lib\FloorCalc\Calculators\Utils\MathUtils;
use App\Lib\FloorCalc\DataContainer;
use \App\Lib\FloorCalc\Exceptions\WrongDataException;

/**
 * Расчёт температур
 * @package App\Lib\FloorCalc\Calculators
 */
class CalcTemperatures
{
    /** @var DataContainer */
    private $data;
    private $heatingFlow;

    /** @var MathUtils */
    private $mathUtils;

    /**
     * CalcTemperatures constructor.
     * @param CalcHeatingFlow $heatingFlow
     */
    public function __construct(CalcHeatingFlow $heatingFlow)
    {
        $this->heatingFlow = $heatingFlow;
        $this->data = $this->heatingFlow->getDataContainer();

        $this->mathUtils = new MathUtils();
    }

    /**
     * тепловой поток по направлению вниз
     * @param float $heatFlowUpwardly
     * @param float $quotientHeatFlux
     * @return float
     */
    public function calcDownwardHeatFlow(float $heatFlowUpwardly, float $quotientHeatFlux): float
    {
        return $heatFlowUpwardly * $quotientHeatFlux;
    }

    /**
     * суммарный удельный тепловой поток
     * @param float $heatFlowUpwardly
     * @param float $downwardHeatFlow
     * @return float
     */
    public function calcTotalSpecificHeatFlux(float $heatFlowUpwardly, float $downwardHeatFlow): float
    {
        return $heatFlowUpwardly + $downwardHeatFlow;
    }

    /**
     * суммарный погонный тепловой поток
     * @param float $totalSpecificHeatFlux
     * @return float
     */
    public function calcTotalLinearHeatFlow(float $totalSpecificHeatFlux): float
    {
        $pipeStep = $this->data->getPipeStep();
        return $totalSpecificHeatFlux * $pipeStep * 0.01;
    }

    /**
     * максимальная температура пола
     * @param float $quotientHeatFlux
     * @param float $heatFlowUpwardly
     * @return float
     * @throws WrongDataException
     */
    public function calcMaxFloorTemperature(float $quotientHeatFlux, float $heatFlowUpwardly) : float
    {
        $tempHeatCarrier = $this->data->getTempHeatCarrier();
        $sumThermalResist = $this->heatingFlow->calcSumThermalResist($this->data->getUpperLayer());
        $pipeStep = $this->data->getPipeStep();
        $leadThermResistanceOfPipes = $this->heatingFlow->getLeadThermResistanceOfPipes();

        return $tempHeatCarrier - (
                $heatFlowUpwardly * (
                    $sumThermalResist +
                    (($pipeStep * 0.01 * $leadThermResistanceOfPipes) /
                        (1 - $quotientHeatFlux))));
    }

    /**
     * минимальная температура пола
     * @param float $maxFloorTemperature
     * @param float $angle
     * @return float
     */
    public function calcMinFloorTemperature(float $maxFloorTemperature, float $angle): float
    {
        $tempInnerRoom = $this->data->getTempInnerRoom();

        return $tempInnerRoom + (($maxFloorTemperature - $tempInnerRoom) * sin(deg2rad($angle)));
    }

    /**
     * средняя  температура пола
     * @param float $maxFloorTemperature
     * @param float $minFloorTemperature
     * @return float
     */
    public function calcAverageFloorTemperature(float $maxFloorTemperature, float $minFloorTemperature): float
    {
        return round(($maxFloorTemperature + $minFloorTemperature) / 2, 1);
    }

}






























