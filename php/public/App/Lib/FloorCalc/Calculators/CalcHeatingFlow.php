<?php


namespace App\Lib\FloorCalc\Calculators;


use App\Lib\Dictionaries\DictionaryManager;
use App\Lib\FloorCalc\Calculators\Utils\MathUtils;
use App\Lib\FloorCalc\DataContainer;
use \App\Lib\FloorCalc\Exceptions\WrongDataException;

class CalcHeatingFlow
{
    private $data;

    /** @var DictionaryManager */
    private $dictionaryManager;

    /** @var MathUtils */
    private $mathUtils;

    public function __construct(DataContainer $data)
    {
        $this->data = $data;
        $this->dictionaryManager = DictionaryManager::getInstance();

        $this->mathUtils = new MathUtils();
    }

    /**
     * геттер для датаконтейнера
     * @return DataContainer
     */
    public function getDataContainer()
    {
        return $this->data;
    }
    /**
     * Вычисляет значение Приведённого термического сопротивления слоёв пола НАД трубами
     * TODO (это результат работы функции ...\Utils\calcLeadThermalResistOfLayer() со значениями по ВЕРХНЕМУ слою)
     * @return float
     * @throws WrongDataException
     */
    public function calcLeadThermalResistUpperLayer(): float
    {
        return $this->mathUtils->calcLeadThermalResistOfLayer(
            $this->calcSumThermalResist(
                $this->data->getUpperLayer()
            ),
            $this->data->getFloorHeatTransferRatio()
        );
    }

    /**
     * Вычисляет значение Приведённого термического сопротивления слоёв пола ПОД трубами
     * TODO (это результат работы функции ...\Utils\calcLeadThermalResistOfLayer() со значениями по НИЖНЕМУ слою)
     * @return float
     * @throws WrongDataException
     */
    public function calcLeadThermalResistLowerLayer(): float
    {
        return $this->mathUtils->calcLeadThermalResistOfLayer(
            $this->calcSumThermalResist(
                $this->data->getLowerLayer()
            ),
            $this->chooseHeatRatioLowerSurface()
        );
    }

    /**
     * Возвращает значение наружного диаметра труб
     * TODO (по функции ...\Dictionaries\getExternalDiameterOfPipes() )
     * @return float
     */
    private function calcExternalDiameterOfPipes(): float
    {
        return $this->dictionaryManager->getMaterialAndSizesOfPipes()->getExternalDiameterOfPipes(
            $this->data->getPipesMaterial(),
            $this->data->getPipesSize()
        );
    }

    /**
     * Возвращает значение наружного диаметра труб
     * TODO (по функции ...\Dictionaries\getInternalDiameterOfPipes() )
     * @return float
     */
    private function calcInternalDiameterOfPipes(): float
    {
        return $this->dictionaryManager->getMaterialAndSizesOfPipes()->getInternalDiameterOfPipes(
            $this->data->getPipesMaterial(),
            $this->data->getPipesSize()
        );
    }

    /**
     * Возващает коэффициент теплоотдачи нижележащей горизонтальной поверхности
     * @return float|null
     */
    private function chooseHeatRatioLowerSurface(): ?float
    {
        $ratioFromForm = $this->data->getHeatRatioLowerSurface();
        $dict = $this->dictionaryManager->getHeatTransferRatio();

        //пока так
        return $dict->getHeatTransferRatio($ratioFromForm);
    }

    /**
     * Суммарное термическое сопротивление слоев
     * @param array $layer
     * @return float
     * @throws WrongDataException
     */
    public function calcSumThermalResist(array $layer): float
    {
        $sum = 0;
        $currentLayer = $layer;

        foreach ($currentLayer as $parametersOfLayer) {

            if (!$parametersOfLayer['thickness']) continue;

            $thickness = $parametersOfLayer['thickness'];
            $ratio = $this->mathUtils->chooseThermalConductivityRatio($parametersOfLayer['ratio'], $parametersOfLayer['material']);
            $sum += $this->mathUtils->calcThermalResist($thickness, $ratio);
        }

        return $sum;
    }

    /**
     * Угол между  поверхностью пола и линией максимального термического сопротивления (вверх)
     * @return float
     */
    public function calcAngle(): float
    {
        $sumThickness = $this->mathUtils->calcSumThicknessOfUpperLayer(
            $this->data->getUpperLayer()
        );

        $externalDiameterOfPipes = $this->calcExternalDiameterOfPipes();

        $pipeStep = $this->data->getPipeStep();

        return rad2deg(atan((2 * ($sumThickness + $externalDiameterOfPipes / 2) / ($pipeStep * 10))));
    }

    /**
     * Максимальное термическое сопротивление слоев над трубой
     * @param float $angle
     * @return float|null
     * @throws WrongDataException
     */
    public function calcMaxThermalResistanceOfAboveLayers(float $angle): ?float
    {
        $thermalResistanceAboveLayer = $this->calcSumThermalResist($this->data->getUpperLayer());

        return $thermalResistanceAboveLayer / (sin(deg2rad($angle)));
    }

    /**
     * Отношение тепловых потоков "низ/верх"
     * @return float|null
     * @throws WrongDataException
     */
    public function calcQuotientHeatFlux(): ?float
    {
        $tempHeatCarrier = $this->data->getTempHeatCarrier();
        $tempLowerRoom = $this->data->getTempLowerRoom();
        $leadThermalResistUpperLayer = $this->calcLeadThermalResistUpperLayer();
        $leadThermalResistLowerLayer = $this->calcLeadThermalResistLowerLayer();
        $externalDiameterOfPipes = $this->calcExternalDiameterOfPipes();
        $internalDiameterOfPipes = $this->calcInternalDiameterOfPipes();
        $ratioOfThermalConductivity = $this->dictionaryManager->getThermalConductivityRatioOfPipes()->getRatioConductivityOfPipesByMaterial($this->data->getPipesMaterial());
        $tempInnerRoom = $this->data->getTempInnerRoom();

        $someRatio = (0.001 * ($externalDiameterOfPipes - $internalDiameterOfPipes) /
                (2 * $ratioOfThermalConductivity)) + (1 / 400);

        return (($tempHeatCarrier - $tempLowerRoom) * ($leadThermalResistUpperLayer + $someRatio)) /
            (($tempHeatCarrier - $tempInnerRoom) * ($leadThermalResistLowerLayer + $someRatio));
    }

    /**
     * Приведенное термическое сопротивление стенок трубы
     * @return float|int
     */
    public function getLeadThermResistanceOfPipes(): ?float
    {
        $externalDiameterOfPipes = $this->calcExternalDiameterOfPipes();
        $internalDiameterOfPipes = $this->calcInternalDiameterOfPipes();
        $ratioOfThermalConductivity = $this->dictionaryManager->getThermalConductivityRatioOfPipes()->getRatioConductivityOfPipesByMaterial($this->data->getPipesMaterial());

        return (1 / (M_PI * 400 * 0.001 * $internalDiameterOfPipes)) +
            ((log($externalDiameterOfPipes / $internalDiameterOfPipes)) / (2 * M_PI * $ratioOfThermalConductivity));
    }

    /**
     * Тепловой поток по направлению вверх
     * @param float $quotientHeatFlux
     * @return float|null
     * @throws WrongDataException
     */
    public function getHeatFlowUpwardly(float $quotientHeatFlux): ?float
    {
        $tempHeatCarrier = $this->data->getTempHeatCarrier();
        $tempInnerRoom = $this->data->getTempInnerRoom();
        $leadThermalResistUpperLayer = $this->calcLeadThermalResistUpperLayer();
        $pipeStep = $this->data->getPipeStep();
        $leadThermResistanceOfPipes = $this->getLeadThermResistanceOfPipes();

        return ($tempHeatCarrier - $tempInnerRoom) /
            ($leadThermalResistUpperLayer + (0.01 * $pipeStep * $leadThermResistanceOfPipes /
                    (1 - $quotientHeatFlux)));
    }
}
