<?php


namespace App\Lib\Dictionaries;

/**
 * Class хранит массив значений материалов труб и их размеры
 * @package App\Lib\Dictionaries
 */
class MaterialAndSizesOfPipes
{
    /**
     * хранит массив значений материалов труб и их размеры
     * @var array
     */
    private $pipes =
        [
            //Сшитый полиэтилен
            'PEX' => [
                'PEX16x2' => [
                    'diameter' => 16,
                    'thickness' => 2,
                ],
                'PEX14x1' => [
                    'diameter' => 14,
                    'thickness' => 1,
                ],
                'PEX12x1' => [
                    'diameter' => 12,
                    'thickness' => 1,
                ],
            ],


            //Металлопластик
            'mePl' => [
                'mePl14x1' => [
                    'diameter' => 14,
                    'thickness' => 1,
                ],
                'mePl12x1' => [
                    'diameter' => 12,
                    'thickness' => 1,
                ],
            ],

            //Полипропилен
            'PPR' => [
                'PPR14x1' => [
                    'diameter' => 14,
                    'thickness' => 1,
                ],
                'PPR12x1' => [
                    'diameter' => 12,
                    'thickness' => 1,
                ],
            ],

            //Медь
            'cop' => [
                'cop14x1' => [
                    'diameter' => 14,
                    'thickness' => 1,
                ],
                'cop12x1' => [
                    'diameter' => 12,
                    'thickness' => 1,
                ],
            ],


        ];

    /**
     * Возвращает значение внутреннего диаметра труб
     * @param string $material
     * @param array $size
     * @return float
     */
    public function getExternalDiameterOfPipes(string $material, array $size): float
    {
        return $this->pipes [$material] [$size[$material]] ['diameter'];
    }

    /**
     * Возвращает значение наружного диаметра труб
     * @param string $material
     * @param array $size
     * @return float
     */
    public function getInternalDiameterOfPipes(string $material, array $size): float
    {
        return $this->getExternalDiameterOfPipes($material, $size) - 2 * $this->pipes[$material][$size[$material]]['thickness'];
    }
}