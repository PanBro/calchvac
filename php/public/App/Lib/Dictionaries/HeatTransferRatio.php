<?php


namespace App\Lib\Dictionaries;


//use App\Lib\FloorCalc\DataContainer;

class HeatTransferRatio
{
    /**
     * хранит коэффициенты теплоотдачи для расчета потерь тепла вниз
     * @var float[]
     */
    private $dictionary = [
      'value1' => 8.7,
      'value2' => 7.6,
    ];


    /**
     * возвращает значение коэффициента теплоотдачи для расчета потерь тепла вниз
     * @param $ratioFromForm
     * @return float
     */
    public function getHeatTransferRatio($ratioFromForm)
    {
        //пока так
        $ratio = $ratioFromForm;
        $dictionary = $this->dictionary;

        return $dictionary['value1'];
    }
}