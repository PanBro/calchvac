<?php


namespace App\Lib\Dictionaries;

/**
 * хранится коэффициент лямбда (λ Вт/м К) -коэффициент теплопроводности каждого слоя
 * @package App\Lib\Dictionaries
 */
class ThermalConductivityRatioOfLayers
{
    /**
     * хранит коэффициенты теплоотдачи матриалов слоёв пола
     * @var float[]
     */
    private $dictionary = [
        'laminate' => 0.29,
        'parquet' => 0.17,
        'tile' => 1.05,

        'CPS' => 0.93,

        'penoplex' => 0.032,
        'polystyrene100' => 0.041,
        'polystyrene150' => 0.05,
        'extrudedPolystyrene' => 0.032,
        'PPU60' => 0.035,
        'PPU80' => 0.041,

        'velox' => 0.11,
        'ZHBI' => 2.04,


        'extrudedPPS' => 0.032,
    ];

    /**
     * возвращает значения коэффициента в зависимости от метериала.
     * @param string $material
     * @return float|null
     */
    public function getRatioByMaterial(string $material): ?float
    {
        return $this->dictionary[$material] ?? null;
    }
}