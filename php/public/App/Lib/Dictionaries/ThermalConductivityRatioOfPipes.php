<?php


namespace App\Lib\Dictionaries;


class ThermalConductivityRatioOfPipes
{
    /**
     * хранит коэффициенты теплоотдачи матриалов труб
     * @var float[]
     */
    private $dictionary = [
        'PEX' => 0.35,
        'mePl' => 0.43,
        'PPR' => 0.24,
        'cop' => 324,
        'steel' => 56,
    ];

    /**
     * возвращает значения коэффициента в зависимости от метериала.
     * @param string $material
     * @return float|null
     */
    public function getRatioConductivityOfPipesByMaterial(string $material): ?float
    {
        return $this->dictionary[$material];
    }
}