<?php


namespace App\Lib\Dictionaries;


class DictionaryManager
{
    /** @var DictionaryManager */
    private static $instance;

    /** @var ThermalConductivityRatioOfLayers */
    private $thermalConductivityRatioOfLayers;
    /** @var HeatTransferRatio */
    private $heatTransferRatio;
    /** @var MaterialAndSizesOfPipes */
    private $pipesMaterialAndSizes;
    /** @var ThermalConductivityRatioOfPipes */
    private $thermalConductivityRatioOfPipes;

    /** DictionaryManager: constructor, clone, wakeup */
    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    /**
     * @return DictionaryManager
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @return ThermalConductivityRatioOfLayers
     */
    public function getThermalConductivityRatioOfLayers(): ThermalConductivityRatioOfLayers
    {
        if ($this->thermalConductivityRatioOfLayers === null) {
            $this->thermalConductivityRatioOfLayers = new ThermalConductivityRatioOfLayers();
        }
        return $this->thermalConductivityRatioOfLayers;
    }

    /**
     * @return HeatTransferRatio
     */
    public function getHeatTransferRatio(): HeatTransferRatio
    {
        if ($this->heatTransferRatio === null) {
            $this->heatTransferRatio = new HeatTransferRatio();
        }
        return $this->heatTransferRatio;
    }

    /**
     * @return MaterialAndSizesOfPipes
     */
    public function getMaterialAndSizesOfPipes(): MaterialAndSizesOfPipes
    {
        if ($this->pipesMaterialAndSizes === null) {
            $this->pipesMaterialAndSizes = new MaterialAndSizesOfPipes();
        }
        return $this->pipesMaterialAndSizes;
    }

    /**
     * @return ThermalConductivityRatioOfPipes
     */
    public function getThermalConductivityRatioOfPipes(): ThermalConductivityRatioOfPipes
    {
        if ($this->thermalConductivityRatioOfPipes === null) {
            $this->thermalConductivityRatioOfPipes = new ThermalConductivityRatioOfPipes();
        }
        return $this->thermalConductivityRatioOfPipes;
    }


}