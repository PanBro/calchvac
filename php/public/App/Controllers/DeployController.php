<?php

namespace App\Controllers;


class DeployController extends AbstractController
{
    // Запускает деплойку
    const COMMAND = '/var/www/calchvac.initschool.com/deploy.sh';

    /**
     * выводит главную страницу
     * @return void
     */
    public function deploy(): void
    {
        $updated = false;
        $updateInfo = '';
        $error = false;
        if (isset($_POST['do'])) {
            $updated = true;
            $output = [];
            $return = 0;
            exec(self::COMMAND, $output, $return);
            $updateInfo = implode(PHP_EOL, $output);
            $error = ($return != 0);
        }

        $this->view(
            'deploypage.html.twig',
            [
                'updated' => $updated,
                'updateInfo' => $updateInfo,
                'error' => $error,
            ]
        );
    }
}