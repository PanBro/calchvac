<?php


namespace App\Controllers;

use App\Base\Routing\Router;

/**
 * Возвращает главную страницу
 * @package App\Controllers
 */
class MainController extends AbstractController
{
    /**
     * выводит главную страницу
     * @return void
     */
    public function index(): void
    {
        $this->view('mainpage.html.twig', []);
    }
}