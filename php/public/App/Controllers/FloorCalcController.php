<?php


namespace App\Controllers;

use App\Lib\FloorCalc\Calculator;
use App\Lib\FloorCalc\DataContainer;
use App\Lib\FloorCalc\DataValidator;
use App\Lib\FloorCalc\Exceptions\WrongDataException;

/**
 * Возвращает выбранный калькулятор тёплого пола
 * @package App\Controllers
 */
class FloorCalcController extends AbstractController
{
    /**
     * запустит калькулятор
     * @param array
     */
    public function actionEasyFloorCalc()
    {
        $this->view('Floorcalc/calc.html.twig', []);
    }

    /**
     * отображает результаты калькулятора
     * @throws WrongDataException
     */
    public function actionRenderEasyCalc()
    {
        $data = new DataContainer($_POST);
        $validator = new DataValidator($data);
        $errors = $validator->validate();

        if ($errors != null) {
            $this->view('Floorcalc/errors.html.twig', ['errors'=>$errors]);
            return;
        }

        $calculator = new Calculator($data);
        $result = $calculator->calculate();
        $this->view('Floorcalc/FloorCalcResult.html.twig', $result);
    }
}