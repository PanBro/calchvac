<?php


namespace App\Controllers;


use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

abstract class AbstractController
{
    /**
     * выводит 404 страницу и выходит
     * TODO сделать заголовки (headers)
     */
    public function show404()
    {
        header('Not found',true,404);
        exit ("<h2>Ошибка 404. Страница НЕ НАЙДЕНА</h2>");
    }

    /**
     * рисует шаблон с нужными данными
     * @param string $template
     * @param array $data
     */
    protected function view(string $template, array $data = []): void
    {
        $loader = new FilesystemLoader(['Views']);
        $twig = new \Twig_Environment($loader);

        try {
            echo $twig->render($template, $data);
        } catch (LoaderError $errException) {
            echo $errException->getMessage();
        } catch (RuntimeError $errException) {
            echo 'ошибка твига';
            echo $errException->getMessage();
        } catch (SyntaxError $errException) {
            $line = $errException->getTemplateLine();
            $file = $errException->getSourceContext()->getName();
            $message = $errException->getMessage();
            echo "Синтаксическая ошибка<br/>{$file}:{$line}<br/> {$message}";
        }
    }
}