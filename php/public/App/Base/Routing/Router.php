<?php


namespace App\Base\Routing;

use App\Controllers\MainController;
use Configs\Routes;

/**
 * соотносит введённые символы в строке браузера с тем, что нужно запускать
 * @package App\Base\Routing
 */
class Router
{
    /** @var string */
    private $link;

    /**
     * распарсенная адресная строка: имя контроллера, имя метода контроллера, имя параметров
     * @var string|array|null
     */
    private $controllerName;
    private $actionName;
    private $parameters;

    /** @var Routes объект Routes */
    private $routes;

    public function __construct(string $link)
    {
        $this->link = empty($link)
            ? $link
            : trim($link, '/');

        $this->routes = new Routes();
    }

    /**
     * распарсивает значение адресной строки согласно конфигу @Routes,
     * в соответствии с этим присваивает значения свойствам:
     * имени контроллера(string), его метода(string) и возможных параметров (array|null)
     */
    private function init()
    {
        if ($this->link == null) {
            $this->controllerName = MainController::class;
            $this->actionName = 'index';
            $this->parameters = [];
            return;
        }

//        var_dump(debug_backtrace());

        foreach ($this->routes->getRoutes() as $uriPattern => $path) {

            $findMatchesInAddressBarWithRoutes = preg_match("~^$uriPattern~", $this->link);
            $internalRoute = preg_replace("~^$uriPattern~", $path, $this->link);

            if (!$findMatchesInAddressBarWithRoutes) {
                continue;
            }

            $findSlashesInAddressBar = preg_match("~/~", $path);

            if (!$findSlashesInAddressBar) {
                $this->fillIfRoutesValueWithoutSlash($internalRoute);
                return;
            }

            $this->fillIfRoutesValueHasSlash($internalRoute);
            return;
//            }
        }

        $this->controllerName = MainController::class;
        $this->actionName = 'show404';
        $this->parameters = [];
    }

    /**
     * заполняет $controllerName, $actionName, $parameters в случае если
     * введённое имя соответствует ключу Routes СО слешем
     * @param string $internalRoute
     */
    private function fillIfRoutesValueHasSlash(string $internalRoute): void
    {
        $segments = explode('/', $internalRoute);
        $this->controllerName = 'App\Controllers\\' . array_shift($segments) . 'Controller';
        $this->actionName = array_shift($segments);
        $this->parameters = $segments;
    }

    /**
     * заполняет $controllerName, $actionName, $parameters в случае если
     * введённое имя соответствует ключу Routes БЕЗ слеша
     * @param string $internalRoute
     */
    public function fillIfRoutesValueWithoutSlash(string $internalRoute): void
    {
        $segments = explode('/', $internalRoute);

        $this->controllerName = 'App\Controllers\\' . array_shift($segments) . 'Controller';
        $this->actionName = 'action' . array_shift($segments);

        $methodsOfActualClass = get_class_methods("$this->controllerName");

        foreach ($methodsOfActualClass as $methodOfClass) {
            $arraysMethodsOfClass[mb_strtolower($methodOfClass)] = "$methodOfClass";
            if (mb_strtolower($methodOfClass) == mb_strtolower($this->actionName)) {
                $this->actionName = $methodOfClass;
                $this->parameters = $segments;
                return;
            }
        }
        $this->controllerName = MainController::class;
        $this->actionName = 'show404';
        $this->parameters = $segments;
    }

    /**
     * возвращает имя контроллера
     * @return string
     */
    public function getControllerName(): string
    {
        if ($this->controllerName === null) {
            $this->init();
        }
        return $this->controllerName;
    }

    /**
     * возвращает имя метода контроллера
     * @return string
     */
    public function getActionName(): ?string
    {
        if ($this->actionName === null) {
            $this->init();
        }
        return $this->actionName;
    }

    /**
     * возвращает параметры (всё указанное в адресной строке после "контроллера") в виде массива, если есть.
     * @return array|null
     */
    public function getParameters(): ?array
    {
        return $this->parameters;
    }
}