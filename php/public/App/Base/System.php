<?php


namespace App\Base;

use App\Base\Routing\Router;
//use App\Controllers\FloorCalcController;
//use App\Controllers\ControllerInterface;
use App\Controllers\MainController;

class System
{

    /** @var Router  */
    public $routing;

    public function __construct()
    {
        $this->routing = new Router($_SERVER['REQUEST_URI'] ?? '');

    }

    public function run()
    {
        //получаем объект контроллера
        $controllerName = $this->routing->getControllerName();
        $controller = new $controllerName();

        //определяем метод
        $actionName = $this->routing->getActionName();

        //проверяем, существует ли он
        $pageExist = method_exists($controller, $actionName);
        if (!$pageExist) {
            (new MainController())->show404();
        }

        //получаем параметры из адреской строки, если они есть
        $params = $this->routing->getParameters();

        //вызываем метод контроллера
        call_user_func([$controller, $actionName], $params);


    }

}