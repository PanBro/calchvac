<?php

namespace Test;
/**
 * Trait TestHelpers
 * Всякие гнусности для unit-тестирования
 */
trait TestHelpers
{
    /**
     * Sets a protected property on a given object via reflection
     *
     * @param $object   - instance in which protected value is being modified
     * @param $property - property on instance being modified
     * @param $value    - new value of the property being modified
     *
     * @return void
     * @throws \ReflectionException
     */
    public function setProtectedProperty($object, $property, $value)
    {
        $reflection = new \ReflectionClass($object);
        $reflectionProperty = $reflection->getProperty($property);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($object, $value);
    }

    /**
     * Calls Protected or private method
     *
     * @param       $object
     * @param       $method
     * @param array $args
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function callProtectedMethod($object, $method, $args = [])
    {
        $method = new \ReflectionMethod($object, $method);
        $method->setAccessible(true);
        return call_user_func_array([$method, 'invoke'], array_merge([$object], $args));
    }

    /**
     * Возвращает приватную константу, если вдруг она понадобится.
     *
     * @param        $object
     * @param string $constantName
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function getPrivateConstant($object, string $constantName)
    {
        $reflection = new \ReflectionClass($object);
        return $reflection->getConstant($constantName);
    }
}
