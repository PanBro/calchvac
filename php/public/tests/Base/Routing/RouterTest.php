<?php

namespace Test\Base\Routing;

use App\Base\Routing\Router;
use App\Controllers\FloorCalcController;
use App\Controllers\MainController;
use PHPUnit\Framework\TestCase;

/**
 * тестирует определение путей
 * @package Test\Base\Routing
 * @group router
 */
class RouterTest extends TestCase
{

    /**
     * @param string $link
     * @param string $expectedController
     * @param string $expectedAction
     * @param array $expectedParameters
     * @dataProvider dataProviderForInit
     */
    public function testInit(string $link, string $expectedController, string $expectedAction, array $expectedParameters)
    {
        $target = new Router($link);
        $actualController = $target->getControllerName();
        $actualAction = $target->getActionName();
        $actualParameters = $target->getParameters();

        static::assertSame($expectedController, $actualController, 'не правильно определился контроллер');
        static::assertSame($expectedAction, $actualAction, 'не правильно определился метод');
        static::assertSame($expectedParameters, $actualParameters, 'не правильно определились параметры');
    }

    public function dataProviderForInit(): array
    {
        return [
            'mainPage' => [
                'link' => '',
                'expectedController' => MainController::class,
                'expectedAction' => 'index',
                'expectedParameters' => [],
            ],
            'easyfloorcalc' => [
                'link' => '/easyfloorcalc/',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' => [],
            ],
            'easyfloorcalcWithOneParam' => [
                'link' => '/easyfloorcalc/step1/',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' => ['step1'],
            ],
            'easyfloorcalcWithMoreParams' => [
                'link' => '/easyfloorcalc/step1/step2/',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' =>
                    [
                        'step1',
                        'step2',
                    ],
            ],
//            'hardfloorcalc' => [
//                'link' => '/hardfloorcalc/',
//                'expectedController' => FloorCalcController::class,
//                'expectedAction' => 'actionHardFloorCalc',
//                'expectedParameters' => [],
//            ],
            'floorcalc' => [
                'link' => '/floorcalc/',
                'expectedController' => MainController::class,
                'expectedAction' => 'show404',
                'expectedParameters' => [],
            ],
            'floorcalcWithEasyMethod' => [
                'link' => '/floorcalc/easyfloorcalc',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' => [],
            ],
//            'floorcalcWithHardMethod' => [
//                'link' => '/floorcalc/hardfloorcalc',
//                'expectedController' => FloorCalcController::class,
//                'expectedAction' => 'actionHardFloorCalc',
//                'expectedParameters' => [],
//            ],
            'floorcalcWithOneParam' => [
                'link' => '/floorcalc/easyfloorcalc/step1/',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' =>
                    ['step1'],
            ],
            'floorcalcWithMoreParams' => [
                'link' => '/floorcalc/easyfloorcalc/step1/step2/',
                'expectedController' => FloorCalcController::class,
                'expectedAction' => 'actionEasyFloorCalc',
                'expectedParameters' =>
                    [
                        'step1',
                        'step2',
                    ],
            ],
            '404floorcalc' => [
                'link' => '/floorcalc/someAddress/',
                'expectedController' => MainController::class,
                'expectedAction' => 'show404',
                'expectedParameters' => [],
            ],
            '404WithoutAnything' => [
                'link' => '/undefinedAddress/',
                'expectedController' => MainController::class,
                'expectedAction' => 'show404',
                'expectedParameters' => [],
            ],
//            'floorcalc' => [
//                'link' => '/floorcalc/',
//                'expectedController' => FloorCalcController::class,
//                'expectedAction' => 'index',
//                'expectedParameters' => [],
//            ],

        ];
    }
}
