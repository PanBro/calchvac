<?php declare(strict_types=1);

namespace Test;

use PHPUnit\Framework\TestCase;

/**
 * Тест проверки работы юнит теста
 *
 * @group   initialunittest
 * @package Test
 */
class InitialTest extends TestCase
{
    /**
     * @param bool $actual   - то, что считает тестируемый объект (конкретно тут такого нет, поэтому передаётся само
     *                       значение)
     * @param bool $excepted - то, что ожидается
     *
     * @dataProvider dataProviderForUnitTest
     */
    public function testUnitTest(bool $actual, bool $excepted): void
    {
        if(defined('PHPUNIT_TEST')){
            echo PHPUNIT_TEST;
        }else{
            echo '---';
        }
        static::assertSame($excepted, $actual, "Похоже что что-то не работает");
    }

    /**
     * @return array
     */
    public function dataProviderForUnitTest()
    {
        return [
            'trueValue'  => [
                'actual'   => true,
                'excepted' => true,
            ],
            'falseValue' => [
                'actual'   => false,
                'excepted' => false,
            ],
        ];
    }
}