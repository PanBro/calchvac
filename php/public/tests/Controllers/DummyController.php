<?php


namespace Test\Controllers;


class DummyController
{
    private $count = 0;
    public function DummyAction()
    {
        $this->count++;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }


}