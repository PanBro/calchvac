<?php

namespace Test\Lib\FloorCalc;

use App\Lib\FloorCalc\DataContainer;
use PHPUnit\Framework\TestCase;

/**
 * тестирует датаконтейнер - на правильное получение значений из формы
 * @package Test\Lib\Floorcalc
 * @group dataContainer
 */
class DataContainerTest extends TestCase
{

    /**
     * @param array $stubbedPost
     * @param float $expectedSize
     * @dataProvider dataProviderForSizeOfFloorGetter
     * @group dataContainer
     */
    public function testGetSizeOfFloor(array $stubbedPost, float $expectedSize)
    {
        $target = new DataContainer($stubbedPost);
        $actualSize = $target->getSizeOfFloor();

        static::assertSame($expectedSize, $actualSize, 'площадь пола определена не верно');
    }

    public function dataProviderForSizeOfFloorGetter()
    {
        return [
            'nullValue' => [
                'stubbedPost' => ['sizeOfFloor' => ''],
                'expectedSize' => 0.0,
            ],
            'commonSize' => [
                'stubbedPost' => ['sizeOfFloor' => 20],
                'expectedSize' => 20.0,
            ],
            'TooBigSize' => [
                'stubbedPost' => ['sizeOfFloor' => 2000],
                'expectedSize' => 2000.0,
            ],
            'tooSmallSize' => [
                'stubbedPost' => ['sizeOfFloor' => 1],
                'expectedSize' => 1.0,
            ],
            'negativeSize' => [
                'stubbedPost' => ['sizeOfFloor' => -10],
                'expectedSize' => -10.0,
            ],
            'stringSize' => [
                'stubbedPost' => ['sizeOfFloor' => 'someStringHere'],
                'expectedSize' => 0.0,
            ],
        ];
    }

    public function testGetPipeStep()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetUpperLayer()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetExtraCalculations()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetTempInnerRoom()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetPipesSize()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetFloorHeatTransferRatio()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetLowerLayer()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetIntermediateCalculations()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetTempLowerRoom()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetPipesMaterial()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetTempHeatCarrier()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }

    public function testGetHeatRatioLowerSurface()
    {
        $this->markTestIncomplete('Этот тест ещё не реализован.');
    }
}
