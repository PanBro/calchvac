<?php

namespace Test\Lib\FloorCalc\Calculators;

use App\Lib\FloorCalc\Calculators\CalcHeatingFlow;
use App\Lib\FloorCalc\Calculators\CalcTemperatures;
use App\Lib\FloorCalc\DataContainer;
use App\Lib\FloorCalc\Exceptions\WrongDataException;
use PHPUnit\Framework\TestCase;

/**
 * Тестирует калькулятор (температуры), класс CalcTemperatures
 * @package Test\Lib\FloorCalc\Calculators
 * @group calc
 */
class CalcTemperaturesTest extends TestCase
{
    /**
     * @param float $heatFlowUpwardly
     * @param float $quotientHeatFlux
     * @param float $expected
     * @dataProvider calcDownwardHeatFlowDataProvider
     */
    public function testCalcDownwardHeatFlow(float $heatFlowUpwardly, float $quotientHeatFlux, float $expected)
    {
        $target = new CalcTemperatures(new CalcHeatingFlow(new DataContainer([])));

        $actual = round($target->calcDownwardHeatFlow(round($heatFlowUpwardly, 0), round($quotientHeatFlux, 2)), 3);

        static::assertSame($expected, $actual, 'функция calcDownwardHeatFlow() работает не правильно');
    }

    public function calcDownwardHeatFlowDataProvider()
    {
        return [
            'commonValue' => [
                'heatFlowUpwardly' => 87.3639344528144,
                'quotientHeatFlux' => 0.119,
                'expected' => 10.44,
            ],
            'AnotherCommonValue' => [
                'heatFlowUpwardly' => 97.3561543056825,
                'quotientHeatFlux' => 0.099,
                'expected' => 9.7,
            ],
        ];
    }

    /**
     * @param float $heatFlowUpwardly
     * @param float $downwardHeatFlow
     * @param float $expected
     * @dataProvider calcTotalSpecificHeatFluxDataProvider
     */
    public function testCalcTotalSpecificHeatFlux(float $heatFlowUpwardly, float $downwardHeatFlow, float $expected)
    {
        $dataContainer = new DataContainer([]);
        $target = new CalcTemperatures(new CalcHeatingFlow($dataContainer));

        $actual = round($target->calcTotalSpecificHeatFlux($heatFlowUpwardly, $downwardHeatFlow), 3);

        static::assertSame($expected, $actual, 'функция calcTotalSpecificHeatFlux() работает не правильно');
    }

    public function calcTotalSpecificHeatFluxDataProvider()
    {
        return [
            'commonValue' => [
                'heatFlowUpwardly' => 87.3639344528144,
                'downwardHeatFlow' => 10.438,
                'expected' => 97.802,
            ],
            'AnotherCommonValue' => [
                'heatFlowUpwardly' => 84.9592627924703,
                'downwardHeatFlow' => 9.617,
                'expected' => 94.576,
            ],
        ];
    }

    /**
     * @param float $totalSpecificHeatFlux
//     * @param float $pipeStep
     * @param DataContainer $data
     * @param float $expected
     * @dataProvider calcTotalLinearHeatFlowDataProvider
     */
    public function testCalcTotalLinearHeatFlow(float $totalSpecificHeatFlux, DataContainer $data, float $expected)
    {
        $target = new CalcTemperatures(new CalcHeatingFlow($data));

        $actual = round($target->calcTotalLinearHeatFlow($totalSpecificHeatFlux), 3);

        static::assertSame($expected, $actual, 'функция calcTotalLinearHeatFlow() работает не правильно');
    }

    public function calcTotalLinearHeatFlowDataProvider()
    {
        return [
            'commonValue' => [
                'totalSpecificHeatFlux' => 94.576,
                'data' => new DataContainer ([
                    'pipeStep' => 20,
                ]),
                'expected' => 18.915,
            ],
            'AnotherCommonValue' => [
                'totalSpecificHeatFlux' => 97.802,
                'data' => new DataContainer([
                    'pipeStep' => 12,
                ]),
                'expected' => 11.736,
            ],
        ];
    }

    /**
     * @param float $quotientHeatFlux
     * @param float $heatFlowUpwardly
     * @param DataContainer $data
     * @param float $expected
     * @throws WrongDataException
     * @dataProvider calcMaxFloorTemperatureDataProvider
     */
    public function testCalcMaxFloorTemperature(
        float $quotientHeatFlux,
        float $heatFlowUpwardly,
        DataContainer $data,
        float $expected
    )
    {
        $target = new CalcTemperatures(new CalcHeatingFlow($data));

        $actual = round($target->calcMaxFloorTemperature($quotientHeatFlux, $heatFlowUpwardly), 3);

        static::assertEqualsWithDelta($expected, $actual, 0.005, 'функция calcMaxFloorTemperature() работает не правильно');
    }

    public function calcMaxFloorTemperatureDataProvider()
    {
        return [
            'commonValues' => [
                'quotientHeatFlux' => 0.119480977675828,
                'heatFlowUpwardly' => 87.3639344528144,
                'data' => new DataContainer([
                    'tempHeatCarrier' => 40.0,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 10, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '', 'material' => 'CPS'],
                        ],
                    'pipeStep' => 12.0,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                ]),
                'expected' => 29.94,
            ],
            'oneMoreCommonValue' => [
                'quotientHeatFlux' => 0.145586650659693,
                'heatFlowUpwardly' => 81.2816094173369,
                'data' => new DataContainer([
                    'tempHeatCarrier' => 45.0,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 40, 'ratio' => '0.31', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '1.1', 'material' => 'CPS'],
                        ],
                    'pipeStep' => 11.0,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX14x1'],
                ]),
                'expected' => 29.39,
            ],

        ];
    }

    /**
     * @param float $maxFloorTemperature
     * @param float $angle
     * @param DataContainer $data
     * @param float $expected
     * @dataProvider calcMinFloorTemperatureDataProvider
     */
    public function testCalcMinFloorTemperature(float $maxFloorTemperature, float $angle, DataContainer $data, float $expected)
    {
        $target = new CalcTemperatures(new CalcHeatingFlow($data));

        $actual = round($target->calcMinFloorTemperature($maxFloorTemperature, $angle), 3);

        static::assertEqualsWithDelta($expected, $actual, 0.005, 'функция calcMinFloorTemperature() работает не правильно');
    }

    public function calcMinFloorTemperatureDataProvider()
    {
        return [
            'commonValues' => [
                'maxFloorTemperature' => 29.9421758593468,
                'angle' => 48.5763343749974,
                'data' => new DataContainer([
                    'tempInnerRoom' => 22,
                ]),
                'expected' => 27.955,
            ],
            'anotherCommonValues' => [
                'maxFloorTemperature' => 31.414975413438,
                'angle' => 60.4463163676926,
                'data' => new DataContainer([
                    'tempInnerRoom' => 25,
                ]),
                'expected' => 30.580,
            ],
        ];
    }

    /**
     * @param float $maxFloorTemperature
     * @param float $minFloorTemperature
     * @param float $expected
     * @dataProvider calcAverageFloorTemperatureDataProvider
     */
    public function testCalcAverageFloorTemperature(float $maxFloorTemperature, float $minFloorTemperature, float $expected)
    {
        $dataContainer = new DataContainer([]);

        $target = new CalcTemperatures(new CalcHeatingFlow($dataContainer));

        $actual = round($target->calcAverageFloorTemperature($maxFloorTemperature, $minFloorTemperature), 1);

        static::assertSame($expected, $actual, 'функция calcAverageFloorTemperature() работает не правильно');
    }

    public function calcAverageFloorTemperatureDataProvider()
    {
        return [
            'commonValues' => [
                'maxFloorTemperature' => 29.9421758593468,
                'minFloorTemperature' => 27.955344113305,
                'expected' => 28.9,
            ],

            'anotherCommonValues' => [
                'maxFloorTemperature' => 32.8306544776221,
                'minFloorTemperature' => 31.9419449000692,
                'expected' => 32.4,
            ],

            'withOneNull' => [
                'maxFloorTemperature' => 0,
                'minFloorTemperature' => 13.0106065306687,
                'expected' => 6.5,
            ],
            'withBothNull' => [
                'maxFloorTemperature' => 0.0,
                'minFloorTemperature' => 0.0,
                'expected' => 0.0,
            ],
        ];
    }
}
