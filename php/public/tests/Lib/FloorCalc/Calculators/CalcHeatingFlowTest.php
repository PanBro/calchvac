<?php

namespace Test\Lib\FloorCalc\Calculators;

use App\Lib\FloorCalc\Calculators\CalcHeatingFlow;
use App\Lib\FloorCalc\Calculators\Utils\MathUtils;
use App\Lib\FloorCalc\DataContainer;
use PHPUnit\Framework\TestCase;
use \App\Lib\FloorCalc\Exceptions\WrongDataException;
use ReflectionException;
use Test\TestHelpers;


/**
 * тестирует калькулятор
 * @package Test\Lib\Floorcalc
 * @group calc
 */
class calcHeatingFlowTest extends TestCase
{
    use TestHelpers;

    /**
     * @param array $layer
     * @param float $expectingResistance
     * @dataProvider calcSumThermalResistDataProvider
     * @throws WrongDataException
     */
    public function testCalcSumThermalResist(array $layer, float $expectingResistance)
    {
        $target = new CalcHeatingFlow (new DataContainer([]));

        $actualResistance = round($target->calcSumThermalResist($layer), 3);

        static::assertSame($expectingResistance, $actualResistance, 'функция calcSumThermalResist() работает не верно');
    }

    /**
     * dataProvider для @testCalcSumThermalResist
     * @return array
     */
    public function calcSumThermalResistDataProvider()
    {
        return [
            'upperLayer' => [
                [
                    '1' => ['thickness' => 10, 'ratio' => '', 'material' => 'laminate'],
                    '2' => ['thickness' => 50, 'ratio' => '', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.088,
            ],
            'upperLayerWithCustomRatios' => [
                [
                    '1' => ['thickness' => 10, 'ratio' => '0.31', 'material' => 'laminate'],
                    '2' => ['thickness' => 50, 'ratio' => '1.1', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.078,
            ],
            'layerOneNullThickness' => [
                [
                    '1' => ['thickness' => 0, 'ratio' => '', 'material' => 'laminate'],
                    '2' => ['thickness' => 100, 'ratio' => '', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.108,
            ],
            'layerOneNullThicknessWithCustomRatios' => [
                [
                    '1' => ['thickness' => 120, 'ratio' => '0.93', 'material' => 'laminate'],
                    '2' => ['thickness' => 0, 'ratio' => '0.032', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.129,
            ],
            'layerWithNullCustomRatios' => [
                [
                    '1' => ['thickness' => 120, 'ratio' => '0.0', 'material' => 'laminate'],
                    '2' => ['thickness' => 0, 'ratio' => '0.0', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.414,
            ],
            'layerBothNullThickness' => [
                [
                    '1' => ['thickness' => 0, 'ratio' => '', 'material' => 'laminate'],
                    '2' => ['thickness' => 0, 'ratio' => '', 'material' => 'CPS'],
                ],
                'expectingResistanceUpperLayer' => 0.0,
            ],

            'lowerLayer' => [
                [
                    '1' => ['thickness' => 170, 'ratio' => '', 'material' => 'extrudedPolystyrene'],
                    '2' => ['thickness' => 25, 'ratio' => '', 'material' => 'velox'],
                ],
                'expectingResistanceLowerLayer' => 5.54,
            ],
        ];
    }

    /**
     * @param int $valueSumThickness
     * @param DataContainer $data
     * @param float $expected
     * @throws ReflectionException
     * @dataProvider dataProviderForCalcAngle
     */
    public function testCalcAngle(int $valueSumThickness, DataContainer $data, float $expected)
    {
        $target = new CalcHeatingFlow ($data);

        $sumThickness = $this->createMock(MathUtils::class);
        $sumThickness->method('calcSumThicknessOfUpperLayer')->willReturn($valueSumThickness);
        $this->setProtectedProperty($target, 'mathUtils', $sumThickness);

        $actual = round($target->calcAngle(), 3);

        static::assertSame($expected, $actual, 'функция calcAngle() работает не правильно');
    }

    /**
     * dataProvider для @testCalcAngle
     * @return array
     */
    public function dataProviderForCalcAngle()
    {
        return [
            'commonValues' => [
                'valueSumThickness' => 60,
                'data' => new DataContainer([
                    'upperLayer' => [],
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                    'pipeStep' => 12.0,
                ]),

                'expected' => 48.576,
            ],
            'anotherCommonValues' => [
                'valueSumThickness' => 110,
                'data' => new DataContainer([
                    'upperLayer' => [],
                    'materialSelects' => 'mePl',
                    'pipesSize' => ['mePl' => 'mePl14x1'],
                    'pipeStep' => 12.5,
                ]),
                'expected' => 61.889,
            ],
            'withNullThickness' => [
                'valueSumThickness' => 0,
                'data' => new DataContainer([
                    'upperLayer' => [],
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                    'pipeStep' => 12.0,
                ]),
                'expected' => 7.595,
            ],
        ];
    }

    /**
     * @param float $angle
     * @param DataContainer $data
     * @param float $expected
     * @throws WrongDataException
     * @dataProvider dataProviderForCalcMaxThermalResistanceOfAboveLayers
     */
    public function testCalcMaxThermalResistanceOfAboveLayers(float $angle, DataContainer $data, float $expected)
    {
//        $target = new CalcHeatingFlow (new DataContainer(['upperLayer' => $layer,]));
        $target = new CalcHeatingFlow ($data);

        $actual = round($target->calcMaxThermalResistanceOfAboveLayers($angle), 3);

        static::assertSame($expected, $actual, 'функция calcMaxThermalResistanceOfAboveLayers() работает не правильно');
    }

    /**
     * dataProvider для testCalcMaxThermalResistanceOfAboveLayers()
     * @return array
     */
    public function dataProviderForCalcMaxThermalResistanceOfAboveLayers()
    {
        return [
            'commonValues' => [
                'angle' => 48.576,
                'data' => new DataContainer([
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 10, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '', 'material' => 'CPS'],
                        ],
                ]),

                'expected' => 0.118,
            ],
            'AnotherCommonValues' => [
                'angle' => 20.0,
                'data' => new DataContainer([
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 10, 'ratio' => '0.31', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '1.1', 'material' => 'CPS'],
                        ],
                ]),
                'expected' => 0.227,
            ],
            'layerOneNullThickness' => [
                'angle' => 60.945,
                'data' => new DataContainer([
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 0, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 100, 'ratio' => '', 'material' => 'CPS'],
                        ],
                ]),
                'expected' => 0.123,
            ],
        ];
    }

    /**
     * @param DataContainer $data
     * @param float $expected
     * @throws WrongDataException
     * @dataProvider dataProviderForCalcQuotientHeatFlux
     */
    public function testCalcQuotientHeatFlux(DataContainer $data, float $expected)
    {
//        $target = new CalcHeatingFlow (
//            new DataContainer([
//                'tempHeatCarrier' => $tempHeatCarrier,
//                'tempLowerRoom' => $tempLowerRoom,
//                'upperLayer' => $upperLayer,
//                'lowerLayer' => $lowerLayer,
//                'floorHeatTransferRatio' => $floorHeatTransferRatio,
//                'materialSelects' => $materialSelects,
//                'pipesSize' => $pipesSize,
//                'tempInnerRoom' => $tempInnerRoom,
//            ])
//        );

        $target = new CalcHeatingFlow ($data);

        $actual = round($target->calcQuotientHeatFlux(), 3);

        static::assertSame($expected, $actual, 'функция calcMaxThermalResistanceOfAboveLayers() работает не правильно');

    }

    public function dataProviderForCalcQuotientHeatFlux()
    {
        return [
            'commonValues' => [
                'data' => new DataContainer([
                    'tempHeatCarrier' => 40,
                    'tempLowerRoom' => -25,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 10, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '', 'material' => 'CPS'],
                        ],
                    'lowerLayer' =>
                        [
                            '1' => ['thickness' => 170, 'ratio' => '', 'material' => 'extrudedPolystyrene'],
                            '2' => ['thickness' => 25, 'ratio' => '', 'material' => 'velox'],
                        ],
                    'floorHeatTransferRatio' => 11,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                    'tempInnerRoom' => 22,
                ]),

                'expected' => 0.119,
            ],
            'AnotherCommonValues' => [
                'data' => new DataContainer([
                    'tempHeatCarrier' => 55,
                    'tempLowerRoom' => -30,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 20, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 40, 'ratio' => '', 'material' => 'CPS'],
                        ],
                    'lowerLayer' =>
                        [
                            '1' => ['thickness' => 200, 'ratio' => '', 'material' => 'extrudedPolystyrene'],
                            '2' => ['thickness' => 40, 'ratio' => '', 'material' => 'velox'],
                        ],
                    'floorHeatTransferRatio' => 11,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                    'tempInnerRoom' => 24,
                ]),

                'expected' => 0.086,
            ],
            'oneMoreLayer' => [
                'data' => new DataContainer([
                    'tempHeatCarrier' => 50,
                    'tempLowerRoom' => -10,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 0, 'ratio' => '1.5', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '4.29', 'material' => 'CPS'],
                        ],
                    'lowerLayer' =>
                        [
                            '1' => ['thickness' => 250, 'ratio' => '0.5', 'material' => 'extrudedPolystyrene'],
                            '2' => ['thickness' => 0, 'ratio' => '', 'material' => 'velox'],
                        ],
                    'floorHeatTransferRatio' => 10,
                    'materialSelects' => 'mePl',
                    'pipesSize' => ['mePl' => 'mePl14x1'],
                    'tempInnerRoom' => 18,
                ]),
                'expected' => 0.352,
            ],
        ];
    }

    /**
     * @param DataContainer $data
     * @param float $expected
     * @dataProvider dataProviderForGetLeadThermResistanceOfPipes
     */
    public function testGetLeadThermResistanceOfPipes(DataContainer $data, float $expected)
    {
        $target = new CalcHeatingFlow ($data);

        $actual = round($target->getLeadThermResistanceOfPipes(), 3);

        static::assertSame($expected, $actual, 'функция getLeadThermResistanceOfPipes() работает не правильно');
    }

    public function dataProviderForGetLeadThermResistanceOfPipes()
    {
        return [
            'commonValues' => [
                'data' => new DataContainer([
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                ]),

                'expected' => 0.197,
            ],
            'AnotherCommonValues' => [
                'data' => new DataContainer([
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                ]),
                'expected' => 0.197,
            ],
            'oneMoreLayer' => [
                'data' => new DataContainer([
                    'materialSelects' => 'mePl',
                    'pipesSize' => ['mePl' => 'mePl14x1'],
                ]),
                'expected' => 0.123,
            ],
        ];
    }

    /**
     * @param float $quotientHeatFlux
     * @param DataContainer $data
     * @param float $expected
     * @throws WrongDataException
     * @dataProvider dataProviderForGetHeatFlowUpwardly
     */
    public function testGetHeatFlowUpwardly(float $quotientHeatFlux, DataContainer $data, float $expected)
    {
        $target = new CalcHeatingFlow($data);

        $actual = round($target->getHeatFlowUpwardly($quotientHeatFlux), 0);

        static::assertSame($expected, $actual, 'функция getHeatFlowUpwardly() работает не правильно');
    }

    public function dataProviderForGetHeatFlowUpwardly()
    {
        return [
            'commonValues' => [
                'quotientHeatFlux' => 0.119,
                'data' => new DataContainer([
                    'tempHeatCarrier' => 40,
                    'tempInnerRoom' => 22,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 10, 'ratio' => '', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '', 'material' => 'CPS'],
                        ],
                    'pipeStep' => 12,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX16x2'],
                    'floorHeatTransferRatio' => 11,
                ]),
                'expected' => 87,
            ],
            'anotherCommonValue' => [
                'quotientHeatFlux' => 0.119,
                'data' => new DataContainer([
                    'tempHeatCarrier' => 40,
                    'tempInnerRoom' => 22,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 40, 'ratio' => '0.31', 'material' => 'laminate'],
                            '2' => ['thickness' => 50, 'ratio' => '1.1', 'material' => 'CPS'],
                        ],
                    'pipeStep' => 10,
                    'materialSelects' => 'PEX',
                    'pipesSize' => ['PEX' => 'PEX12x1'],
                    'floorHeatTransferRatio' => 11,
                ]),
                'expected' => 63,
            ],
            'as5d8f5a6sd5f6as5d8a' => [
                'quotientHeatFlux' => 0.173,
                'data' => new DataContainer([
                    'tempHeatCarrier' => 40,
                    'tempInnerRoom' => 22,
                    'upperLayer' =>
                        [
                            '1' => ['thickness' => 0, 'ratio' => '10', 'material' => 'laminate'],
                            '2' => ['thickness' => 80, 'ratio' => '', 'material' => 'CPS'],
                        ],
                    'pipeStep' => 10,
                    'materialSelects' => 'mePl',
                    'pipesSize' => ['mePl' => 'mePl14x1'],
                    'floorHeatTransferRatio' => 12,
                ]),
                'expected' => 98,
            ],
        ];
    }
}
