<?php

namespace Test\Lib\FloorCalc\Calculators\Utils;

use App\Lib\FloorCalc\Calculators\Utils\MathUtils;
use PHPUnit\Framework\TestCase;
use \App\Lib\FloorCalc\Exceptions\WrongDataException;

/**
 * Class MathUtilsTest
 * @package Test\Lib\FloorCalc\Calculators\Utils
 * @group calc
 */
class MathUtilsTest extends TestCase
{
    /**
     * @param int $thickness
     * @param float|null $ratio
     * @param float $expected
     * @throws WrongDataException
     * @dataProvider dataProviderForCalcThermalResist
     */
    public function testCalcThermalResist(int $thickness, ?float $ratio, float $expected)
    {
        $target = new MathUtils();
        $actual = $target->calcThermalResist($thickness, $ratio);

        static::assertSame($expected, $actual, 'функция calcThermalResist работает не правильно');
    }

    public function dataProviderForCalcThermalResist()
    {
        return [
            'usual' => [
                'thickness' => 10,
                'ratio' => 0.29,
                'expected' => 0.03448275862069,
            ],
            'anotherUsual' => [
                'thickness' => 170,
                'ratio' => 0.032,
                'expected' => 5.3125,
            ],
            'withNull' => [
                'thickness' => 0,
                'ratio' => 0.032,
                'expected' => 0.0,
            ],
        ];
    }

    /**
     * тест на исключения
     * @param int $thickness
     * @param float|null $ratio
     * @throws WrongDataException
     * @dataProvider dataProviderForCalcThermalResistException
     */
    public function testCalcThermalResistException(int $thickness, ?float $ratio)
    {
        $target = new MathUtils();

        static::expectException(WrongDataException::class);
        $target->calcThermalResist($thickness, $ratio);
    }

    public function dataProviderForCalcThermalResistException()
    {
        return [
            'withNull' => [
                'thickness' => 170,
                'ratio' => null,
            ],
            'emptyValue' => [
                'thickness' => 170,
                'ratio' => 0,
            ],
        ];
    }

    /**
     * @param array $layer
     * @param int $expected
     * @dataProvider dataProviderForSumThicknessOfUpperLayer
     */
    public function testCalcSumThicknessOfUpperLayer(array $layer, int $expected)
    {
        $target = new MathUtils();
        $actual = $target->calcSumThicknessOfUpperLayer($layer);

        static::assertSame($expected, $actual, 'функция calcSumThicknessOfUpperLayer работает не правильно');
    }

    public function dataProviderForSumThicknessOfUpperLayer()
    {
        return [
            'casual' => [
                'layer' =>
                    [
                        ['thickness' => 10,],
                        ['thickness' => 50,],
                    ],
                'expected' => 60,
            ],
            'anotherCasual' => [
                'layer' =>
                    [
                        ['thickness' => 170,],
                        ['thickness' => 25,],
                    ],
                'expected' => 195,
            ],
            'withOneLayer' => [
                'layer' =>
                    [
                        ['thickness' => 50,],
                    ],
                'expected' => 50,
            ],
            'casuffal' => [
                'layer' =>
                    [
                    ],
                'expected' => 0,
            ],

        ];
    }

    /**
     * выбирает значение коэффициента термического сопротивления
     * @param float|null $byUser
     * @param string $fromDictionary
     * @param float|null $expectedResist
     * @dataProvider dataContainerForChooseThermalConductivityRatio
     */
    public function testChooseThermalConductivityRatio($byUser, string $fromDictionary, ?float $expectedResist)
    {
        $target = new MathUtils();

        $actualResist = $target->chooseThermalConductivityRatio($byUser, $fromDictionary);

        static::assertSame($expectedResist, $actualResist, 'функция chooseThermalConductivityRatio работает не правильно');

    }

    public function dataContainerForChooseThermalConductivityRatio()
    {
        return [
            'withBothFilled' => [
                'byUser' => '0.29',
                'fromDictionary' => 'laminate',
                'expectedResist' => 0.29,
            ],
            'withBothFilledAgain' => [
                'byUser' => 0.192,
                'fromDictionary' => 'CPS',
                'expectedResist' => 0.192,
            ],
            'selectMenuIsEmpty' => [
                'byUser' => '',
                'fromDictionary' => 'laminate',
                'expectedResist' => 0.29,
            ],

            'bothEmpty' => [
                'byUser' => 0.0,
                'fromDictionary' => '',
                'expectedResist' => 0.0,
            ],

        ];
    }

    /**
     * Приведённое термическое сопротивление слоёв пола НАД трубами
     * @param float $thermalResist
     * @param float $floorHeatTransferRatio
     * @param float $expectedResist
     * @dataProvider dataContainerForCalcLeadThermalResistOfUpperLayer
     * @throws WrongDataException
     */
    public function testCalcLeadThermalResistOfUpperLayer(float $thermalResist, float $floorHeatTransferRatio, float $expectedResist)
    {
        $target = new MathUtils();

        $actualResist = round($target->calcLeadThermalResistOfLayer($thermalResist, $floorHeatTransferRatio), 3);

        static::assertSame($expectedResist, $actualResist, 'функция calcLeadThermalResistOfLayer работает не правильно относительно ВЕРХНИХ слоёв пола');
    }

    public function dataContainerForCalcLeadThermalResistOfUpperLayer()
    {
        return [
            'goodUpperLayer' => [
                'thermalResist' => 0.088,
                'floorHeatTransferRatio' => 11.0,
                'expectedResist' => 0.179,
            ],
            'goodAnotherUpperLayer' => [
                'thermalResist' => 0.192,
                'floorHeatTransferRatio' => 10.0,
                'expectedResist' => 0.292,
            ],
            'withNullResist' => [
                'thermalResist' => 0.0,
                'floorHeatTransferRatio' => 10.0,
                'expectedResist' => 0.1,
            ],

        ];
    }

    /**
     * Приведённое термическое сопротивление слоёв пола ПОД трубами
     * @param float $thermalResist
     * @param float $floorHeatTransferRatio
     * @param float $expectedResist
     * @dataProvider dataContainerForCalcLeadThermalResistOfLowerLayer
     * @throws WrongDataException
     */
    public function testCalcLeadThermalResistOfLowerLayer(float $thermalResist, float $floorHeatTransferRatio, float $expectedResist)
    {
        $target = new MathUtils();

        $actualResist = round($target->calcLeadThermalResistOfLayer($thermalResist, $floorHeatTransferRatio), 3);

        static::assertSame($expectedResist, $actualResist, 'функция calcLeadThermalResistOfLayer работает не правильно относительно НИЖНИХ слоёв пола');

    }

    public function dataContainerForCalcLeadThermalResistOfLowerLayer()
    {
        return [
            'goodLowerLayer' => [
                'thermalResist' => 5.54,
                'floorHeatTransferRatio' => 8.7,
                'expectedResist' => 5.655,
            ],
            'goodAnotherLowerLayer' => [
                'thermalResist' => 4.92,
                'floorHeatTransferRatio' => 7.6,
                'expectedResist' => 5.052,
            ],
            'withNullResist' => [
                'thermalResist' => 0.0,
                'floorHeatTransferRatio' => 8.7,
                'expectedResist' => 0.115,
            ],

        ];
    }
}
