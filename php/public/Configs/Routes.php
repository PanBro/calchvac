<?php


namespace Configs;

use App\Controllers\MainController;

/**
 * хранит массив введённого пути в строку браузера - с названиями контроллеров и методов
 * @package App\Base\Routing\Routes
 */
class Routes
{

    /**
     * возвращает названия контроллера и метода по введённому uri
     * @return string[]
     */
    public function getRoutes(): array
    {
        return
            [

                'easyfloorcalc' => 'FloorCalc/actionEasyFloorCalc',
//                 в классе FloorCalcController используем метод actionEasyFloorCalc

                'floorcalcresult' => 'FloorCalc/actionRenderEasyCalc',
//                 в классе FloorCalcController используем метод actionRenderEasyCalc


                'floorcalc' => 'FloorCalc',
                'deploy' => 'Deploy/deploy',

            ];
    }

    /**
     * возвращает названия контроллеров по умолчанию
     * @return string
     */
    public function getDefaultController(): string
    {
        return MainController::class;
    }

    /**
     * возвращает названия методов по умолчанию
     * @return string
     */
    public function getDefaultAction(): string
    {
        return 'index';
    }
}